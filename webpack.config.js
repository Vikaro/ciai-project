var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');


var config = {
   devtool: debug ? "inline-sourcemap" : null,
   plugins: debug ? [] : [
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
   ],


   entry: "./js/client.js",
   output: {
      path:'./',
      filename: 'index.min.js',
   },


   devServer: {
      inline: true,
      port: 8080
   },
	
   module: {
      loaders: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel',
				
            query: {
               presets: ['es2015', 'react']
            }
         }
      ]
   }
}

module.exports = config;