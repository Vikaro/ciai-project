/**
 * Created by Mateusz on 27.10.2016.
 */
import React from "react";
import { Link } from "react-router";


import Footer from "../components/layout/Footer";
import Nav from "../components/layout/Nav";

export default class Layout extends React.Component {
    render() {
        const { location } = this.props;
        console.log("layout");
        return (
            <div>

                {/*<Nav location={location} />*/}

                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <h1>New USOS</h1>

                            {this.props.children}

                        </div>
                    </div>
                    {/*<Footer/>*/}
                </div>
            </div>

        );
    }
}