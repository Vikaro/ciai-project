/**
 * Created by Mateusz on 29.10.2016.
 */
import React from 'react';
import CourseInformation from '.././CourseComponents/CourseInformation';
import CourseGrades from '.././CourseComponents/CourseGrades';

export default class StudentCourse extends React.Component {

    constructor(props) {
        super(props);

        var idCourse = this.props.params.id;
        console.log(idCourse);
        var students_list = require("json!../../../students.json");
        console.log(students_list);
        var courseInfo = students_list.courses.filter(function(el){
            return el.number === idCourse;
        });
        var courseGrades = students_list.grades;
        console.log(courseInfo);
        this.state = {
            courseInfo,
            courseGrades
        };

        console.log(this.state.courseInfo);
    }
    render() {
        return (
            <div>
                <div><CourseInformation course_info = {this.state.courseInfo[0]}/></div>
                <div><CourseGrades grades = {this.state.courseGrades}/> </div>
                {/*this.state.students.map((person, i) => <TableRow key = {i} data = {person} />)*/}
            </div>
        );
    }
}
