/**
 * Created by stili on 10/29/2016.
 */
import React from 'react';
import DegreeTable from './Tables/DegreeTable';
import CourseInformation from './CourseComponents/CourseInformation';
class CourseView extends React.Component {

    constructor(props) {
        var courses_list = require("json!../../courses.json");
        super(props);
        console.log(this.props.params.name);
        this.state = courses_list;
    }
    render() {
        return (
            <div>
                <Header name={this.props.params.name}/>
                <div><DegreeTable courses={this.state.courses} /></div>
                <div>...</div>
                <div>Thesis: 60 <br></br>
                    Total ECTS: 240</div>
            </div>
        );
    }
}

class Header extends React.Component {
    render() {
        return (
            <div>
                <h1>{this.props.name.toUpperCase()}</h1>
            </div>
        );
    }
}


export default CourseView;