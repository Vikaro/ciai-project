import React from 'react';
import FilterableTable from './Tables/StudentsTable';
import CourseInformation from './CourseComponents/CourseInformation';
import CourseList from './Tables/CourseList';

class CourseView extends React.Component {

    constructor(props) {
        var students_list = require("json!../../students.json");
        console.log(students_list);
        super(props);
        this.state = students_list;
    }
    render() {
        return (
            <div>
                <Header/>
                <div><CourseInformation course_info = {this.state.courses[0]}/></div>
                {/*this.state.students.map((person, i) => <TableRow key = {i} data = {person} />)*/}
                <div><FilterableTable students={ this.state.students} /></div>
                <div><CourseList courses={this.state.courses} /></div>
            </div>
        );
    }
}

class Header extends React.Component {
    render() {
        return (
            <div>
                <h1>Students List</h1>
            </div>
        );
    }
}

class TableRow extends React.Component {
    render() {
        return (
            <tr>
                <td>{this.props.data.id}</td>
                <td>{this.props.data.name}</td>
                <td>{this.props.data.age}</td>
            </tr>
        );
    }
}

export default CourseView;