/**
 * Created by stili on 10/28/2016.
 */
import React from "react";

// import Router from "react-router"
export default class CourseInformation extends React.Component {

    constructor(props) {
        super(props);
    };

    render(){
        return (
        <div>
            <CourseID name={this.props.course_info.name} number={this.props.course_info.number}/>
         <b>    {this.props.course_info.years} </b>
            <br/> <b>ECTS:</b> {this.props.course_info.ECTS}
            <Teacher teachers={this.props.course_info.teachers}/>
            <CourseDescription description = {this.props.course_info.description}/>
            <br/><br/>
        </div>
        )
    };
}

class CourseID extends React.Component {
    render() {
        return (
            <div>
                <h1>{this.props.name}({this.props.number} )</h1>
            </div>
        );
    }
}

class Teacher extends React.Component {
    render() {
        return (
            <div>
                <p><b>Teachers:</b> {this.props.teachers}</p>
            </div>
        );
    }
}


class CourseDescription extends React.Component {
    render() {
        return (
            <div>
                <h3>Short course description: <br/> </h3>
                    {this.props.description}
            </div>
        );
    }
}