/**
 * Created by Mateusz on 29.10.2016.
 */
import React from "react";

export default class CourseGrades extends React.Component {

    constructor(props) {
        super(props);
    };

    render() {
        var rows = [];
        var rk = 0; // row key
        var average = 0;
        this.props.grades.forEach((el) => {
            // id.courses.forEach(function(courseId) {
            average += el.value
            rows.push(<CourseRow name={el.name} value = {el.value} key={rk++} />);

        });

        average /= rk;
        return (
            <div>
                <table className="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Test: </th>
                        <th>Value: </th>
                    </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>

                <p> <b>Average grade: <h4>{average}</h4> </b></p>
            </div>
        )
    }
}

class CourseRow extends React.Component {
    render() {
        var name = this.props.name;
        var value = this.props.value;
        return (
            <tr>
                <td>{name}</td>
                <td>{value}</td>
            </tr>

        );
    }
}
