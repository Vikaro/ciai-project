/**
 * Created by Mateusz on 28.10.2016.
 */
import React from "react";
import CourseView from "./CourseView";
// import Router from "react-router"
export default class Professor extends React.Component {

    constructor() {
        super();
        this.state = {
            fields : ["id", "name"]
        };

    };

    render(){
        return (
            <div>
               <p> Hello Professor, {this.props.params.name} </p>
                <CourseView used={this.state.fields}/>
            </div>
        )
    }

}/**
 * Created by Mateusz on 28.10.2016.
 */
