/**
 * Created by Mateusz on 28.10.2016.
 */
import React from "react";
import {Link} from "react-router";
import YearsTable from "./Tables/YearsTable"

export default class Student extends React.Component {

    constructor() {
        super();
        var students_list = require("json!../../students.json");

        this.state = {
            students_list
        };


    };

    render(){
        return (
            <div>
                <div className="col-md-2">
                <h3> Hello, {this.props.params.name} </h3>
                <Link to="student/Joan/edit" className="btn btn-primary" >Account details</Link>
                </div>
                <div className="col-md-10">
                    <YearsTable courses={this.state.students_list.courses}/>
                    <Link to="degrees/informatics" className="btn btn-primary btn-sm btn-block">Informatics curriculum</Link>
                </div>
            </div>

        )
    }
}


var YEARS = [
    {id:"0", year: '2016-2017',  courses: [{id : 0, courseName:  'CP'}, {id : 1, courseName:  'CIAI'}]},
    {id:"1", year: '2015-2016',  courses: [{id : 4, courseName:  'Programming'}, {id : 7, courseName:  'Logic'}]},
];