/**
 * Created by Mateusz on 27.10.2016.
 */
import React from "react";
import {Link } from "react-router";

export default class Login extends React.Component {

    constructor() {
        super();
        this.state = {
            login : "",
            password: ""
        };

        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
    };


    handleChangeLogin(event) {
        this.setState({login: event.target.value});
    }
    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }
    render(){
        return(
            <div>
                <div className="col-md-4"></div>
                <div className="col-md-4">
                    <p>Login: </p>
                    <input type="text" className="form-control"  value={this.state.login} onChange={this.handleChangeLogin} />
                    <p>Password: </p>
                    <input type="password" className="form-control" value={this.state.password} onChange={this.handleChangePassword}/>

                    <Link to="student/Joan" className="btn btn-primary btn-lg btn-block" >Login as student(debug)</Link>
                    <Link to="professor/Ruj" className="btn btn-primary btn-lg btn-block" >Login as professor(debug)</Link>
                </div>
                <div className="col-md-4"></div>
            </div>
        )
    }
}