/**
 * Created by stili on 10/29/2016.
 */
import React from 'react';
import {Link} from "react-router";

class TableSemesterRow extends React.Component {
    render() {
        return (<tr><th colSpan="2">Semester: {this.props.semester}</th></tr>);
    }
}

class TableRow extends React.Component {
    render() {
        var name = this.props.course.name;
        var number = this.props.course.number;
        var ects = this.props.course.ECTS;
        var coursesRoute = "/courses/";

        var link = encodeURI(coursesRoute + number);
        return (
            <tr>
                <td> {number} </td>
                <td> {name} </td>
                <td> {ects}</td>
                <td><Link to={link}> More </Link></td>
            </tr>
        );
    }
}

class DegreeTable extends React.Component {
    constructor(props){
        super(props);
        console.log(this.props.courses);
    };

    render() {
        var rows = [];
        var semesters = new Set();
        var semesterId = 0;
        this.props.courses.forEach((course) => {
            if (!semesters.has(course.semester)) {
                rows.push(<TableSemesterRow semester= {course.semester} key={semesterId++} />);
            }
            rows.push(<TableRow course={course} key={course.number} />);
            semesters.add(course.semester);
        });
        return (
            <table className="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Number</th>
                    <th>Name</th>
                    <th>ECTS</th>
                </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }

}
export default DegreeTable;