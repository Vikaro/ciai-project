import React from 'react';
import {Link} from "react-router"
import SearchBox from './SearchBox';

class CourseEntry extends React.Component {
    render() {
        return (
            <tr>
                <td>{this.props.course.number}</td>
                <td>{this.props.course.name}</td>
                <td>{this.props.course.years}</td>
            </tr>
        );
    }
}

class CourseTable extends React.Component {
    render() {
        var rows = [];
        this.props.courses.forEach((course) => {
            if (course.name.indexOf(this.props.filterText) === -1){
                return;
            }
            rows.push(<CourseEntry course={course} key={course.number} />);
        });
        return (
            <table>
                <thead>
                <tr>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Year</th>
                </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
}


class FilterableCourseTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filterText: '',
        };

        this.handleUserInput = this.handleUserInput.bind(this);
    }

    handleUserInput(filterText) {
        this.setState({
            filterText: filterText,
        });
    }

    render() {
        return (
            <div>
                <SearchBox
                    filterText={this.state.filterText}
                    onUserInput={this.handleUserInput}
                />
                <CourseTable
                    courses={this.props.courses}
                    filterText={this.state.filterText}
                />
            </div>
        );
    }
}

class StudentEntry extends React.Component {
  render() {
    return (
      <tr>
        <td>{this.props.student.id}</td>
        <td>{this.props.student.name}</td>
        <td>{this.props.student.email}</td>
        <td>Mark</td>
        <td>Delete</td>
      </tr>
    );
  }
}

class StudentTable extends React.Component {
  render() {
    var rows = [];
    this.props.students.forEach((student) => {
      if (student.name.indexOf(this.props.filterText) === -1){
        return;
      }
      rows.push(<StudentEntry student={student} key={student.id} />);
    });
    return (
        <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
              <th>Email</th>
              <th>Mark</th>
              <th>Enroled</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}

class FilterableStudentTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: '',
    };
    
    this.handleUserInput = this.handleUserInput.bind(this);
  }

  handleUserInput(filterText) {
    this.setState({
      filterText: filterText,
    });
  }

  render() {
    return (
      <div>
        <SearchBox
          filterText={this.state.filterText}
          onUserInput={this.handleUserInput}
        />
        <StudentTable
          students={this.props.students}
          filterText={this.state.filterText}
        />
      </div>
    );
  }
}

export default FilterableStudentTable;