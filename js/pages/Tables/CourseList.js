import React from 'react';
import SearchBox from './SearchBox';

class CourseEntry extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            editNumber: "-1",
            updated: false
        };
        this.changeStatus = this.changeStatus.bind(this);
        this.changeUpdated = this.changeUpdated.bind(this);
    }

    changeStatus() {
        this.setState({editNumber: this.props.course.id});
        this.setState({updated: false});
    }

    changeUpdated() {
        this.setState({updated: true});
    }

    render() {
        if (this.props.course.id == this.state.editNumber && !this.state.updated) {
            return (
                <tr>
                    <td><textarea ref="new_number">{this.props.course.number}</textarea></td>
                    <td><textarea ref="new_name">{this.props.course.name}</textarea></td>
                    <td><textarea ref="new_year">{this.props.course.years}</textarea></td>
                    <td><textarea ref="new_description">{this.props.course.description}</textarea></td>
                    <td><button onClick={this.changeUpdated}>SAVE</button></td>
                </tr>
            );
        } else if (this.state.updated) {
            return (
                <tr>
                    <td>{this.refs.new_number.value}</td>
                    <td>{this.refs.new_name.value}</td>
                    <td>{this.refs.new_year.value}</td>
                    <td>{this.refs.new_description.value}</td>
                    <td><button onClick={this.changeStatus}>EDIT</button></td>
                </tr>
            );
        } else {
            return (
                <tr>
                    <td>{this.props.course.number}</td>
                    <td>{this.props.course.name}</td>
                    <td>{this.props.course.years}</td>
                    <td>{this.props.course.description}</td>
                    <td><button onClick={this.changeStatus}>EDIT</button></td>
                </tr>
            );
        }

    }
}

class CourseTable extends React.Component {
    render() {
        var rows = [];
        this.props.courses.forEach((course) => {
            if (course.name.indexOf(this.props.filterText) === -1){
                return;
            }
            rows.push(<CourseEntry course={course} key={course.number} />);
        });
        return (
            <table className="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Year</th>
                    <th>Description</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
}


export default class FilterableCourseTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filterText: '',
        };

        this.handleUserInput = this.handleUserInput.bind(this);
    }

    handleUserInput(filterText) {
        this.setState({
            filterText: filterText,
        });
    }

    render() {
        return (
            <div>
                <SearchBox
                    filterText={this.state.filterText}
                    onUserInput={this.handleUserInput}
                />
                <CourseTable
                    courses={this.props.courses}
                    filterText={this.state.filterText}
                />
            </div>
        );
    }
}