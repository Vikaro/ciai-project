/**
 * Created by Mateusz on 28.10.2016.
 */
/**
 * Created by Mateusz on 28.10.2016.
 */
import React from "react"
import {Link} from "react-router"

class YearRow extends React.Component {
    render() {
        return (
            <tr><th colSpan="2">{this.props.year}</th></tr>
        );
    }
}

class CourseRow extends React.Component {
    render() {
        var name = this.props.name;
        var id = this.props.id;
        var  currentRoute = "/student/Joan/course/";

        var link = encodeURI(currentRoute + id);
        return (
            <tr>
                <td><Link to={link}> {name} </Link></td>
            </tr>
        );
    }
}


class CourseTable extends React.Component {

    render(){
        var rows = [];
        var lastYear = null;
        var rk = 0; // row key
        this.props.courses.forEach((id) => {

            // if (course.name.indexOf(this.props.filterText) === -1) {
            //     return;
            // }
            if (id.years !== lastYear) {
                rows.push(<YearRow year={id.years} key={rk++} />);
            }
            // id.courses.forEach(function(courseId) {
            rows.push(<CourseRow id={id.number} name={id.name} key={rk++} />);

            // });
            lastYear = id.years;
        });
        return (
            <div>
                <table className="table table-striped table-hover">
                    <thead>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
            </div>
        )
    }
}



export default class SchoolYears extends React.Component {

    render(){
        return(
            <div>
                <CourseTable courses={this.props.courses} />
            </div>

        )

    }
}
