/**
 * Created by Mateusz on 27.10.2016.
 */
import React from "react";
import ReactDOM from "react-dom";
import {Link} from "react-router";

import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Student from "./pages/Student"
import Professor from "./pages/Professor"
import Layout from "./pages/Layout";
import Login from "./pages/Login"
import Edit from "./pages/Student/Edit"
import StudentCourse from "./pages/Student/StudentCourse"
import Degree from "./pages/Degree";
import CourseFromUrl from "./pages/CourseComponents/CourseFromUrl";

const app = document.getElementById('app');

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={Layout}>
            <IndexRoute component={Login}> </IndexRoute>

            <Route path="/student/:name" name="student" component={Student}> </Route>
            <Route path="/student/:name/edit" name="edit" component={Edit}> </Route>
            <Route path="/student/:name/course/:id" name="course" component={StudentCourse}> </Route>
            <Route path="/courses/:number" name="course" component={CourseFromUrl}> </Route>
            <Route path="/professor/:name" name="professor" component={Professor}> </Route>
            <Route path="/degrees/:name" name="name" component={Degree}> </Route>
            {/*<IndexRoute component={Featured}></IndexRoute>*/}
            {/*<Route path="archives(/:article)" name="archives" component={Archives}></Route>*/}
            {/*<Route path="settings" name="settings" component={Settings}></Route>*/}
        </Route>
    </Router>,
    app);